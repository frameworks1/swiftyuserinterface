//
//  ContentView.swift
//  SwiftyUserInterface
//
//  Created by Imthath M on 28/03/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    let array = ["First", "Second", "Third"]
//    @State var switchable = false
    @State var string = ""
    
    var body: some View {
//        List(array, id: \.self) { string in
//            Card(alignment: .leading, cornerRadius: .small, shadowRadius: .zero) {
//                Text(string)
//            }
//        }.introspectTableView {
//            $0.separatorStyle = .none
//            $0.tableFooterView = UIView()
//        }
        
//        SecureField("test", text: $string)
        
//        LoginView(idTitle: "User name or Mobile number",
//                  idPlaceholder: "Enter your user name or mobile number",
//                  buttonAction: login(id:password:), logoContent: {
//            Image(systemName: "car.fill")
//                .font(.system(size: 72))
//        })
        
//        PasscodeField(maxDigits: 6, label: "Enter Passcode", handler: submit(pin:onCompletion:))
        
        AllFeatures()
        
//        TextField("Enter password", text: $password)
//            .keyboardType(.numberPad)
//            .textFieldStyle(RoundedBorderTextFieldStyle())
//            .introspectTextField { $0.becomeFirstResponder() }
        
        
//            CheckList(checkpointTexts: array, completed: 2)
        
//        VProgress(checkpointTexts: array, completed: 2)
        
//        Toggle(isOn: $switchable, label: {
//            Text("testing")
//            } )
//            .toggleStyle(CheckedToggle())
//            .disabled(true)
    }
    
    private func submit(pin: String, onCompletion handler: (Bool) -> Void) {
//        sleep(1)
        handler(true)
    }
    
    private func login(id: String, password: String) {
        print("\(id) and \(password)")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
//        Group {
//            ContentView().environment(\.colorScheme, .dark)
            ContentView()
//                .previewLayout(.sizeThatFits)
//        }
        
    }
}

struct AllFeatures: View {

        let array = ["First", "Second", "Third"]
    //    @State var switchable = false
        @State var string = ""

        
        private func submit(pin: String, onCompletion handler: (Bool) -> Void) {
    //        sleep(1)
            handler(true)
        }
        
        private func login(id: String, password: String) {
            print("\(id) and \(password)")
        }
    
    var body: some View {
        
        NavigationView {
            VStack(spacing: .large) {
                NavigationLink(destination: LoginView(idTitle: "User name or Mobile number",
                          idPlaceholder: "Enter your user name or mobile number",
                          buttonAction: login(id:password:), logoContent: {
                    Image(systemName: "car.fill")
                        .font(.system(size: 72))
                })) {
                    Text("LoginView")
                }
                
//                NavigationLink(destination: PasscodeField(maxDigits: 6, label: "Enter Passcode", handler: submit(pin:onCompletion:))) {
//                    Text("OTP")
//                }
                
                NavigationLink(destination: VProgress(checkpointTexts: array, completed: 3), label: {
                    Text("Verticla progress")
                })
                
                NavigationLink(destination: List(array, id: \.self) { string in
                    Card(alignment: .leading, cornerRadius: .small, fillColor: .red, shadowRadius: .zero) {
                        Text(string)
                    }
                }.introspectTableView {
                    $0.separatorStyle = .none
                    $0.tableFooterView = UIView()
                }, label: {
                    Text("Cards")
                })
            }
        }
    }
}
