//
//  AdaptiveKeyboard.swift
//  SwiftyUserInterface
//
//  Created by Imthath M on 14/04/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI
import Combine

//public extension View {
//    func keyboardAdaptive() -> some View {
//        ModifiedContent(content: self, modifier: KeyboardAdaptive())
//    }
//}
//
//struct KeyboardAdaptive: ViewModifier {
//    @State private var bottomPadding: CGFloat = 0
//    
//    func body(content: Content) -> some View {
//        GeometryReader { geometry in
//            content
//                .padding(.bottom, self.bottomPadding)
//                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
//                    
//                    // top-most position of the keyboard in the global coordinate space
//                    let keyboardTop = geometry.frame(in: .global).height - keyboardHeight + .averageTouchSize
//                    
//                    // bottom-most position of the focused text field
//                    let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
//                    
//                    // the intersection size between the keyboard and text field,
//                    // conisering bottom safe area inset
//                    self.bottomPadding = max(0, focusedTextInputBottom - keyboardTop - geometry.safeAreaInsets.bottom)
//            }
//            .animation(.easeOut(duration: 0.16))
//        }
//    }
//}
//
//extension Publishers {
//    
//    // can never fail with an error
//    static var keyboardHeight: AnyPublisher<CGFloat, Never> {
//        
//        let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
//            .map { $0.keyboardHeight + .averageTouchSize }
//        
//        let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
//            .map { _ in CGFloat(0) }
//        
//        // Combine multiple publishers into one by merging their emitted values
//        return MergeMany(willShow, willHide)
//            .eraseToAnyPublisher()
//    }
//}
//
//extension Notification {
//    var keyboardHeight: CGFloat {
//        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
//    }
//}
//
//// From https://stackoverflow.com/a/14135456/6870041
//
//extension UIResponder {
//    
//    static var currentFirstResponder: UIResponder? {
//        _currentFirstResponder = nil
//        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
//        return _currentFirstResponder
//    }
//
//    private static weak var _currentFirstResponder: UIResponder?
//
//    @objc private func findFirstResponder(_ sender: Any) {
//        UIResponder._currentFirstResponder = self
//    }
//
//    var globalFrame: CGRect? {
//        guard let view = self as? UIView else { return nil }
//        return view.superview?.convert(view.frame, to: nil)
//    }
//}

public struct SearchField: View {

    @Binding var searchText: String
    @State private var showCancelButton: Bool = false

    var foregroundColor: Color = .primary
    var backgroundColor: Color = Color(.secondarySystemBackground)
    let addCancelButton: Bool
    let app: UIApplication
    
    public init(searchText: Binding<String>,
                backgroundColor: Color,
                app: UIApplication, addCancelButton: Bool = true) {
        self._searchText = searchText
        self.backgroundColor = backgroundColor
        self.app = app
        self.addCancelButton = addCancelButton
    }

    @ViewBuilder public var body: some View {
        hStack
    }
    
    var hStack: some View {
        HStack {
            HStack(spacing: .zero) {
                Image(systemName: "magnifyingglass").padding(.trailing, .small)

                TextField("Search", text: $searchText, onEditingChanged: { isEditing in
                    self.showCancelButton = true
                })
                    //.foregroundColor(foregroundColor)

                Spacer()
                Button(action: {
                    self.searchText = ""
                }) {
                    Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                }
            }
            .padding(EdgeInsets(top: .small, leading: .small * 0.75, bottom: .small, trailing: .small * 0.75))
            .foregroundColor(.secondary)
            .background(backgroundColor)
            .cornerRadius(.small)

            if addCancelButton && showCancelButton  {
                Button(action: {
                    self.app.dismissKeyboard() // this must be placed before the other commands here
                    self.searchText = ""
                    self.showCancelButton = false
                }, label: {
                    Text("Cancel")
                        .font(.footnote)
                        .foregroundColor(.accentColor)
                })
//                .animation(.easeInOut)
            }
        }
        .animation(.easeInOut)
//        .padding(.small)
    }
}

public extension UIApplication {

    func dismissKeyboard() {
        let keyWindow = connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
        keyWindow?.endEditing(true)
    }
}
