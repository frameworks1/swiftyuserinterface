//
//  AuthViews.swift
//  SwiftyUserInterface
//
//  Created by Imthath M on 12/04/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI

public struct LoginView<Logo>: View where Logo: View {
    
    @State var idField: UITextField? = nil
    @State var passwordField: UITextField? = nil
    @State var isFirstAppearance = true
    var idTitle: String = "Email"
    var idPlaceholder: String = "Enter your email address"
    
    var passwordTitle: String = "Password"
    var passwordPlaceholder: String = "Enter your password"
    
    @State var identifier: String = ""//1234567890"
    @State var password: String = ""//123456"
    
    var textBoxColor: Color = .secondary
    
    var buttonTitle: String = "Submit"
    var buttonTitleColor: Color = Color(UIColor.secondarySystemBackground)
    var buttonBackground: Color = Color(UIColor.label)
    
    var buttonAction: (String, String) -> Void
    
    var logoContent: () -> Logo
    
    public init(idTitle: String = "Email",
                idPlaceholder: String = "Enter your email address",
                passwordTitle: String = "Password",
                passwordPlaceholder: String = "Enter your password",
                textBoxColor: Color = .secondary,
                buttonTitle: String = "Submit",
                buttonTitleColor: Color = Color(UIColor.secondarySystemBackground),
                buttonBackground: Color = Color(UIColor.label),
                buttonAction: @escaping (String,String) -> Void,
                logoContent: @escaping () -> Logo) {
        self.idTitle = idTitle
        self.idPlaceholder = idPlaceholder
        self.passwordTitle = passwordTitle
        self.passwordPlaceholder = passwordPlaceholder
        self.textBoxColor = textBoxColor
        self.buttonTitle = buttonTitle
        self.buttonTitleColor = buttonTitleColor
        self.buttonBackground = buttonBackground
        self.buttonAction = buttonAction
        self.logoContent = logoContent
    }
    
    public var body: some View {
        loginStack
//        GeometryReader { geometry in
//            ScrollView(showsIndicators: false) {
//                self.loginStack.frame(minHeight: geometry.size.height)
//            }.introspectScrollView { $0.bounces = false }
//        }
    }
    
    private var loginStack: some View {
        VStack {
            logoContent().padding([.bottom, .top], .averageTouchSize)
            StyleBox(title: idTitle, placeholder: idPlaceholder,
                     value: $identifier, boxColor: textBoxColor,
                     background: Color(UIColor.systemBackground),
                     onCommit: editPassword).introspectTextField { textField in
                        self.idField = textField
                        if self.isFirstAppearance {
                            textField.becomeFirstResponder()
                            self.isFirstAppearance = false
                        }
            }
            
            StyleBox(title: passwordTitle, placeholder: passwordPlaceholder,
                     value: $password, boxColor: textBoxColor,
                     background: Color(UIColor.systemBackground),
                     type: .secure,
                onCommit: submit).introspectTextField { textField in
                    self.passwordField = textField
                }
                .padding(.bottom, .averageTouchSize)
            Button(action: submit, label: buttonView)
                .frame(height: .averageTouchSize * 1.25)
//                .padding(.bottom, .averageTouchSize * 4)
//        }.introspectTableView { tableView in
//            tableView.separatorStyle = .none
//            tableView.backgroundColor = .systemBackground
        }
    }
    
    private func submit() {
        buttonAction(identifier, password)
    }
    
    private func buttonView() -> some View {
        Card(cornerRadius: .small, fillColor: buttonBackground, shadowRadius: .zero) {
            Text(self.buttonTitle).foregroundColor(self.buttonTitleColor)
        }.padding([.leading, .trailing], .small)
    }
    
    private func editUsername() {
        idField?.becomeFirstResponder()
    }
    
    private func editPassword() {
        passwordField?.becomeFirstResponder()
    }
}


public struct PasscodeField: View {
    
    var maxDigits: Int = 4
    var label = "Enter One Time Password"
    
    @Binding var pin: String
    @State var showPin = false
    @State var isDisabled = false
    
    
    var handler: (@escaping (Bool) -> Void) -> Void
    
    public init(label: String = "Enter One Time Password",
                pin: Binding<String>,
                handler: @escaping (@escaping (Bool) -> Void) -> Void) {
        self.label = label
        self._pin = pin
        self.handler = handler
    }
    
    public var body: some View {
        VStack(spacing: .large) {
            Text(label).font(.title)
            ZStack {
                pinDots
                backgroundField
            }
            showPinStack
        }
        
    }
    
    private var pinDots: some View {
        HStack {
            Spacer()
            ForEach(0..<maxDigits) { index in
                Image(systemName: self.getImageName(at: index))
                    .font(.system(size: .large, weight: .thin, design: .default))
                Spacer()
            }
        }
    }
    
    private var backgroundField: some View {
        let boundPin = Binding<String>(get: { self.pin }, set: { newValue in
            self.pin = newValue
            self.submitPin()
        })
        
        return TextField("", text: boundPin, onCommit: submitPin)
            .introspectTextField { textField in
                textField.tintColor = .clear
                textField.textColor = .clear
//                textField.keyboardType = .numberPad
//                textField.becomeFirstResponder()
//                textField.isEnabled = !self.isDisabled
        }
    }
    
    private var showPinStack: some View {
        HStack {
            Spacer()
            if !pin.isEmpty {
                showPinButton
            }
        }
        .frame(height: .averageTouchSize)
        .padding([.trailing])
    }
    
    private var showPinButton: some View {
        Button(action: {
            self.showPin.toggle()
        }, label: {
            self.showPin ?
                Image(systemName: "eye.slash.fill").foregroundColor(.primary) :
                Image(systemName: "eye.fill").foregroundColor(.primary)
        })
    }
    
    private func submitPin() {
        guard !pin.isEmpty,
            pin.count == maxDigits else {
                //            showPin = false
                // this code is never reached under  normal circumstances. If the user pastes a text with count higher than the
                // max digits, we remove the additional characters and make a recursive call.
                if pin.count > maxDigits {
                    pin = String(pin.prefix(maxDigits))
                    submitPin()
                }
                return
        }
        
//            isDisabled = true
            
            handler { isSuccess in
                if isSuccess {
                    debugPrint("CRASH - passcode field succces")
//                    "pin matched, go to next page, no action to perfrom here".log()
                } else {
                    debugPrint("CRASH - passcode field failure")
                    self.pin = ""
//                    self.isDisabled = false
//                    "this has to called after showing toast why is the failure".log()
                }
            }
    }
    
    private func getImageName(at index: Int) -> String {
        if index >= self.pin.count {
            return "circle"
        }
        
        if self.showPin {
            return self.pin.digits[index].numberString + ".circle"
        }
        
        return "circle.fill"
    }
}
