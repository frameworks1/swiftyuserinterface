//
//  StylingViews.swift
//  SwiftyUserInterface
//
//  Created by Imthath M on 29/03/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI

public struct CardButtonStyle: ButtonStyle {
    
    let backgroundColor: Color
    let textColor: Color
    let height: CGFloat
    
    public init(backgroundColor: Color = .blue,
                textColor: Color = .white,
                height: CGFloat = .averageTouchSize * 1.25) {
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.height = height
    }
    
    public func makeBody(configuration: Configuration) -> some View {
        Card(cornerRadius: .small, fillColor: bgColor(for: configuration)) {
            configuration.label.foregroundColor(self.fgColor(for: configuration))
        }
        .frame(height: height)
        .padding(.vertical, .small)
    }
    
    func fgColor(for configuration: Configuration) -> Color {
        configuration.isPressed ? textColor.opacity(0.6) : textColor
    }
    
    func bgColor(for configuration: Configuration) -> Color {
        configuration.isPressed ? backgroundColor.opacity(0.3) : backgroundColor
    }
}

public struct CheckedToggle: ToggleStyle {
    
    var color = Color.green
    
    public func makeBody(configuration: Self.Configuration) -> some View {
        HStack(spacing: .large) {
            Button(action: { configuration.isOn.toggle() } )
            {
                if configuration.isOn {
                    Image(systemName: "checkmark.circle.fill")
                        .foregroundColor(color)
                } else {
                    Image(systemName: "circle")
                        .foregroundColor(Color(UIColor.secondaryLabel))
                }
                
            }
            configuration.label
            Spacer()
        }
        .padding(.horizontal)
    }
}

public class AnyGestureRecognizer: UIGestureRecognizer {
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        state = .began
    }

    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
       state = .ended
    }

    override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        state = .cancelled
    }
}


public struct HighlightedText: View {
    let text: String
    let matching: String

    public init(_ text: String, matching: String) {
        self.text = text
        self.matching = matching
    }

    public var body: some View {
        let tagged = text.replacingOccurrences(of: self.matching,
                                               with: "<SPLIT>>\(self.matching)<SPLIT>")//, options: .caseInsensitive)
        
        let split = tagged.components(separatedBy: "<SPLIT>")
        return split.reduce(Text("")) { (a, b) -> Text in
            guard !b.hasPrefix(">") else {
                return a + Text(b.dropFirst()).foregroundColor(.red)
            }
            return a + Text(b)
        }
    }
}

public struct ExpandableView<SelectionView: View, ExpandedView: View>: View {

//    @Environment(\.colorScheme) var colorScheme
    
    var selectionView: SelectionView
    var expandedView: ExpandedView
    var backgroundColor: Color
    var canAddPadding: Bool
    
    @State var expandView: Bool
    
    public init(selectionView: SelectionView, expandedView: ExpandedView,
         backgroundColor: Color = Color(.secondarySystemGroupedBackground),
         openExpanded: Bool = false, addPadding: Bool = true) {
        self.selectionView = selectionView
        self.expandedView = expandedView
        self.backgroundColor = backgroundColor
        self._expandView = State(initialValue: openExpanded)
        self.canAddPadding = addPadding
    }
    
    public var body: some View {
        VStack(alignment: .center, spacing: .zero) {
            HStack {
                selectionView
                arrowMark
                    .imageScale(.small)
                    .foregroundColor(Color(.tertiaryLabel))
            }
                // using medium padding for trailing, small for the arrowMark and small for the HStack
            .padding(EdgeInsets(top: .medium, leading: .small, bottom: .medium, trailing: .medium))
            .contentShape(Rectangle())
            .onTapGesture(perform: toggleView)
            if expandView {
//                ZStack {
//                    Color(colorScheme == .dark ? .systemGray4 : .systemGray5)
                    expandedView
//                }
            }
        }
        .background(backgroundColor)
        .cornerRadius(.small)
        .padding(overallPadding)
    }
    
    var overallPadding: EdgeInsets {
        guard canAddPadding else {
            return EdgeInsets(CGFloat.zero)
        }
        
        return EdgeInsets(top: .small, leading: .medium, bottom: .small, trailing: .medium)
    }
    
    var arrowMark: Image {
        if expandView {
            return Image(systemName: "chevron.up")
        }
        
        return Image(systemName: "chevron.down")
    }
    
    func toggleView() {
        withAnimation(.easeInOut(duration: 0.3), {
            self.expandView.toggle()
        })
    }
}
