//
//  UIExtensions.swift
//  SDComponents
//
//  Created by Imthath M on 11/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI

public extension EdgeInsets {
    init(_ side: CGFloat) {
        self.init(top: side, leading: side, bottom: side, trailing: side)
    }
}

public extension GeometryProxy {
    var height: CGFloat { size.height }
    var width: CGFloat { size.width }
}

public extension UITableView {
    
    func format() {
        self.backgroundColor = .clear
        self.separatorStyle = .none
        let footerView = UIView()
        footerView.backgroundColor = .clear
        self.tableFooterView = footerView
    }
}

public extension View {
    
    func stacked(for device: Device) -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return AnyView(self.navigationViewStyle(StackNavigationViewStyle()))
        } else {
            return AnyView(self.navigationViewStyle(DoubleColumnNavigationViewStyle()))
        }
    }
    
    func rectangleBackground<T: View>(with color: T) -> some View {
        self
             .padding(EdgeInsets(top: .medium, leading: .small, bottom: .medium, trailing: .small))
            .background(color)
            .cornerRadius(.small)
            .padding(EdgeInsets(top: .small, leading: .medium, bottom: .small, trailing: .medium))
    }
    
    func formatTableView(_ tableView: UITableView) {
        tableView.format()
    }
    
    func alternateLoader(on isLoading: Binding<Bool>) -> some View {
        Group {
            if isLoading.wrappedValue {
                ActivityIndicator.large.padding([.top, .bottom], .averageTouchSize * 5)
            } else {
                self
            }
        }
    }
    
    func toggle(flag: Binding<Bool>, withLabel string: String) -> Toggle<Text> {
        Toggle(isOn: flag, label: { Text(string) })
    }
    
    func makeTag(with color: Color) -> some View {
        self
        .padding(EdgeInsets(top: .small * 0.5, leading: .small, bottom: .small * 0.5, trailing: .small))
        .background(color)
        .cornerRadius(.small * 1.5)
    }
    
    func secondaryText() -> some View {
        self
            .font(.footnote)
            .foregroundColor(.secondary)
            .multilineTextAlignment(.leading)
    }
    
    func push<T: View>(to destination: T, isPushed: Binding<Bool>) -> some View {
        ZStack {
            NavigationLink(destination: destination, isActive: isPushed, label: { EmptyView() }).opacity(0)
            self
        }
    }
    
    func centerHorizontally() -> some View {
        HStack {
            Spacer()
            self
            Spacer()
        }
    }
    
    var asAnyView: AnyView {
        AnyView(self)
    }
    
    func simpleAlert(isPresented: Binding<Bool>, title: String = "Alert", message: String) -> some View {
        return self.alert(isPresented: isPresented, content: {
            Alert(title: Text(title), message: Text(message))
        })
    }
    
    func embedInScrollView(canShowIndicators showsIndicators: Bool = false,
                           canBounce bounces: Bool = false) -> some View {
        GeometryReader { geometry in
            ScrollView(showsIndicators: showsIndicators) {
                self
            }
            .frame(minHeight: geometry.size.height)
            .introspectScrollView { $0.bounces = bounces }
        }
    }
    
    func toast(isShowing: Binding<Bool>, message: Binding<String>) -> some View {
        Toast(isShowing: isShowing,
              presenting: { self },
              message: message)
    }
    
    func toggleVisibility(basedOn condition: Bool) -> some View {
        self.opacity(condition ? 1 : 0)
    }
    
    func textButton(withTitle title: String, andAction action: @escaping () -> Void) -> some View {
        Button(action: action, label: {
            Text(title)
        })
    }
    
    func imageButton(withName name: String, size: CGFloat = .averageTouchSize * 0.5,
                     andAction action: @escaping () -> Void) -> some View {
        Button(action: action, label: {
            Image(systemName: name).font(.system(size: size, weight: .regular))
        }).frame(width: .averageTouchSize, height: .averageTouchSize)
    }
    
    func fieldAndButton(placeholder: String, value: Binding<String>,
                        title: String, action: @escaping () -> Void) -> some View {
        HStack {
            TextField(placeholder, text: value)
                .background(roundedSecondaryBackground)
            textButton(withTitle: title, andAction: action)
        }.padding(.leading, 20).padding(.trailing, 20)
    }
    
    var roundedSecondaryBackground: some View {
        Color(UIColor.secondarySystemBackground)
            .cornerRadius(3)
            .padding(EdgeInsets(top: -5, leading: -5, bottom: -5, trailing: 0))
    }
    
    func simpleToast(isShowing showToast: Binding<Bool>) -> some View {
        ZStack(alignment: .bottom, content: {
            self
            if showToast.wrappedValue {
                Text("testing toast")
                .onAppear(perform: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        print("timing over")
                    })
                })
            }
            
        })
    }
}

public extension CGFloat {
    static var small: CGFloat { 8 }
    static var medium: CGFloat { 16 }
    static var large: CGFloat { 24 }
    
    static var averageTouchSize: CGFloat { 44 }
    static var imageSize: CGFloat { 72 }
}

public extension BinaryInteger {
    
    func isSimilar(to other: Self) -> Bool {
        String(other).contains(String(self))
    }
}
