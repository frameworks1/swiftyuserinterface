//
//  CustomizedViews.swift
//  SwiftyUserInterface
//
//  Created by Imthath M on 29/03/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI
import Introspect

public struct ScaledImage: View {
    
    let systemName: String
    let scale: Image.Scale
    let minWidth: CGFloat
    
    public init(systemName: String, scale: Image.Scale = .large, minWidth: CGFloat = .averageTouchSize) {
        self.systemName = systemName
        self.scale = scale
        self.minWidth = minWidth
    }
    
    public var body: some View {
        Image(systemName: systemName)
            .imageScale(scale)
            .frame(minWidth: minWidth)
    }
}

public struct DismissButton: View {
    var title: String = "Cancel"
    var presentationMode: Binding<PresentationMode>
    
    public init(title: String = "Cancel",
                presentationMode: Binding<PresentationMode>) {
        self.title = title
        self.presentationMode = presentationMode
    }
    public var body: some View {
        Button(title) {
            self.presentationMode.wrappedValue.dismiss()
        }
    }
}

public struct CheckList: View {
    
    var checkpointTexts: [String]
    var completed: Int
    
    public var body: some View {
        VStack(spacing: .zero) {
            ForEach(0..<checkpointTexts.count) { index in
                Toggle(isOn: .constant(index < self.completed)) {
                    Text(self.checkpointTexts[index])
                }
                .toggleStyle(CheckedToggle())
                .disabled(true)
            }
        }
        
    }
}

public struct VProgress: View {
    
    var checkpointTexts: [String]
    var completed: Int
    var completedImageString: String = "checkmark.circle"
    var height: CGFloat = .averageTouchSize * 2
    
    public init(checkpointTexts: [String],
                completed: Int,
                completedImageString: String = "checkmark.circle",
                height: CGFloat = .averageTouchSize * 2) {
        self.checkpointTexts = checkpointTexts
        self.completed = completed
        self.completedImageString = completedImageString
        self.height = height
    }
    
    public var body: some View {
        VStack(spacing: .zero) {
            ForEach(0..<checkpointTexts.count) { index in
                self.getRow(at: index)
            }
        }
    }
    
    private func getRow(at index: Int) -> some View {
        HStack(alignment: .top) {
            getIcon(at: index).frame(width: .averageTouchSize, height: height)
            Text(checkpointTexts[index]).offset(x: 0, y: -2)
            Spacer()
        }
    }
    
    private func getIcon(at index: Int) -> some View {
        ZStack(alignment: .top) {
            Rectangle()
                .fill(self.getLineColor(for: index))
                .opacity(0.5)
                .frame(width: 2)
                .offset(x: 0, y: 5)
            getImage(at: index)
            .padding(2)
                .background(Color(UIColor.systemBackground))
        }
    }
    
    private func getLineColor(for index: Int) -> Color {
        if index == checkpointTexts.count-1 {
            return .clear
        } else if index < completed-1 {
            return .green
        }
        
        return Color(UIColor.quaternaryLabel)
    }
    
    private func getImage(at index: Int) -> some View {
        if index < completed {
            return Image(systemName: completedImageString)
                .foregroundColor(.green)
        }
        
        return Image(systemName: "circle")
            .foregroundColor(Color(UIColor.quaternaryLabel))
    }
}

public struct HScroll<Content>: View where Content: View {
    
    var view: Content
    
    public var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                view
            }
        }
    }
    
    public init(@ViewBuilder bodyBuilder: () -> Content) {
        view = bodyBuilder()
    }
}

public enum TextFieldType {
    case plain
    case secure
    
    /// on commit block in sytlebox will not work for this type
    // TODO: fix it - undetsand and change the implementation of UITextViewWrapper
    // which is used inside MultiLineTextField
    case multiline
}

public struct StyleBox: View {
    
    var title: String
    var placeholder: String
    
    @Binding var value: String
    
    var keyboardType: UIKeyboardType = UIKeyboardType.default
    var boxColor: Color = Color.secondary
    var background: Color = Color(UIColor.secondarySystemGroupedBackground)
//    var isSecureFiled: Bool = false
    var type: TextFieldType = .plain
    
    var onCommit: () -> Void = {}
    
    public init(title: String,
                placeholder: String, value: Binding<String>,
                keyboardType: UIKeyboardType = UIKeyboardType.default,
                boxColor: Color = Color.secondary,
                background: Color = Color(UIColor.secondarySystemGroupedBackground),
//                isSecureFiled: Bool = false,
        type: TextFieldType = .plain,
                onCommit: @escaping () -> Void = { }) {
        self.title = title
        self.placeholder = placeholder
        self._value = value
        self.keyboardType = keyboardType
        self.boxColor = boxColor
        self.background = background
//        self.isSecureFiled = isSecureFiled
        self.type = type
        self.onCommit = onCommit
    }
    
    public var body: some View {
        ZStack(alignment: .topLeading) {
            textField
                .keyboardType(keyboardType)
                .padding(EdgeInsets(top: .medium, leading: .medium, bottom: .medium, trailing: .small))
                .overlay(RoundedRectangle(cornerRadius: .small * 0.5).stroke(boxColor))
            
            // when the title is empty, which should not be for most cases,
            // you can use a text field with RoundedBorderTextFieldStyle
            // use this only when you have other same elements in your view and
            // you want the formatting to be same for all text fields
            if !title.isEmpty {
                Text(title)
                    .padding([.leading, .trailing], 5)
                    .background(background)
                    .foregroundColor(boxColor).font(.footnote)
                    .offset(x: .small, y: -.small)
            }
        }.padding(EdgeInsets(top: .medium, leading: .small, bottom: .small, trailing: .small))
        .background(background)
        
    }
    
    var textField: some View {
//        Group {
            switch type {
            case .plain:
                return AnyView(TextField(placeholder, text: $value, onCommit: onCommit))
            case .secure:
                return AnyView(SecureField(placeholder, text: $value, onCommit: onCommit))
            case .multiline:
                return AnyView(MultilineTextField(placeholder, text: $value, onCommit: nil))
            }
//            switch type == .secure {
//                SecureField(placeholder, text: $value, onCommit: onCommit)
//            } else {
//                TextField(placeholder, text: $value, onCommit: onCommit)
//            }
//        }
    }
}


extension String {
    
    var digits: [Int] {
        var result = [Int]()
        
        for char in self {
            if let number = Int(String(char)) {
                result.append(number)
            }
        }
        
        return result
    }
    
}

extension Int {
    
    var numberString: String {
        
        guard self < 10 else { return "0" }
        
        return String(self)
    }
}

extension Optional {
    var isNil: Bool {
        switch self {
        case .none:
            return true
        case .some:
            return false
        }
    }
}

struct Custom_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(buttonAction: { (_, _ ) in }, logoContent: { Text("Enter your details")})
    }
}
