//
//  Loader.swift
//  ASAP_Demo
//
//  Created by Imthath M on 02/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import SwiftUI
import UIKit

public struct CoverView<Top, Bottom>: View where Top: View, Bottom: View {
    
    var topView: Top
    @Binding var isVisible: Bool
    var bottomView: () -> Bottom
    
    public init(topView: Top, isVisible: Binding<Bool>, @ViewBuilder bottomView: @escaping () -> Bottom) {
        self.topView = topView
        self._isVisible = isVisible
        self.bottomView = bottomView
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.bottomView()
                    .disabled(self.isVisible)
                    .opacity(self.isVisible ? 0 : 1)
                self.topView
                    .frame(width: geometry.size.width , height: geometry.size.height)
                    .opacity(self.isVisible ? 1 : 0)
            }
        }
    }
    
}

public struct Loader<Content>: View where Content: View {
    
    @Binding var isVisible: Bool
    var content: () -> Content
    
    public init(isVisible: Binding<Bool>, @ViewBuilder content: @escaping () -> Content) {
        self._isVisible = isVisible
        self.content = content
    }

    public var body: some View {
        CoverView(topView: ActivityIndicator.large, isVisible: $isVisible, bottomView: content)
    }
    
}

public struct ErrorCover<Content, AnyError>: View where Content: View, AnyError: Error {

    @State var showsError: Bool = false
    @Binding var error: AnyError? {
        didSet {
            showsError = !error.isNil
        }
    }

    var content: () -> Content

    public init(error: Binding<AnyError?>, @ViewBuilder content: @escaping () -> Content) {
        self._error = error
        self.content = content
    }

    public var body: some View {
        CoverView(topView: ErrorView($error), isVisible: $showsError, bottomView: content)
    }
}

public struct ErrorView<AnyError>: View where AnyError: Error {

    @Binding public var error: AnyError?

    public init(_ error: Binding<AnyError?>) {
        self._error = error
    }

    public var body: some View {
        VStack {
            if error.isNil {
                EmptyView()
            } else {
                Text("\(error!.localizedDescription)")
            }
        }
    }
}

public struct ActivityIndicator: UIViewRepresentable {
    
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    public func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    public func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}


public struct Toast<Presenting>: View where Presenting: View {
    
    /// The binding that decides the appropriate drawing in the body.
    @Binding var isShowing: Bool
    /// The view that will be "presenting" this toast
    let presenting: () -> Presenting
    /// The text to show
    
    //    let text: Text
    @Binding var message: String
    
    public var body: some View {
        
//        GeometryReader { geometry in
            
            ZStack(alignment: .bottom) {
                
                self.presenting()
//                    .blur(radius: self.isShowing ? 1 : 0)
                
//                VStack {
                    //                    self.text
                if self.isShowing {
                        Text(self.message)
                        .onAppear {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    withAnimation {
                                        self.isShowing = false
                                    }
                                }
                        }
                    }
                    
//                }
//                .frame(width: geometry.size.width / 2,
//                       height: geometry.size.height / 5,
//                       alignment: .bottom)
//                    .background(Color.secondary.colorInvert())
//                    .foregroundColor(Color.primary)
//                    .cornerRadius(20)
//                    .transition(.slide)
//
//                .hidden()
//                .opacity(self.isShowing ? 1 : 0)
            }
            
//        }
        
    }
    
}

public extension ActivityIndicator {
    static var large: some View  {
        ActivityIndicator(isAnimating: .constant(true), style: .large)
    }
}

public struct Card<Content>: View where Content: View {
    
    var alignment: Alignment = .center
    var cornerRadius: CGFloat = .medium
    var fillColor: Color = Color(UIColor.secondarySystemBackground)
    var shadowRadius: CGFloat = .zero
    var padding: CGFloat = .small    
    var content: () -> Content
    
    public init(alignment: Alignment = .center,
                cornerRadius: CGFloat = .medium,
                fillColor: Color = Color(UIColor.secondarySystemBackground),
                shadowRadius: CGFloat = .zero,
                padding: CGFloat = .small,
                content: @escaping () -> Content) {
        self.alignment = alignment
        self.cornerRadius = cornerRadius
        self.fillColor = fillColor
        self.shadowRadius = shadowRadius
        self.padding = padding
        self.content = content
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: self.alignment) {
                RoundedRectangle(cornerRadius: self.cornerRadius, style: .continuous)
                    .fill(self.fillColor)
//                    .fill(RadialGradient(gradient: Gradient(colors: [self.fillColor]), center: .center, startRadius: 2, endRadius: 200))
                    .shadow(radius: self.shadowRadius)

                self.content()
                .padding(self.padding)
            }
            .frame(width: geometry.size.width, height: geometry.size.height)
        }
    }
}

public extension View {
    
    func makeCard(alignment: Alignment = .center,
              cornerRadius: CGFloat = .medium,
              fillColor: Color = Color(UIColor.secondarySystemBackground),
              shadowRadius: CGFloat = .zero,
              padding: CGFloat = .small) -> some View {
        Card(alignment: alignment, cornerRadius: cornerRadius, fillColor: fillColor, shadowRadius: shadowRadius, padding: padding, content: {
            self
        })
    }
}

struct Stack_Previews: PreviewProvider {
    static var previews: some View {
        Card {
            Text("Sample full size card")
        }
    }
}
